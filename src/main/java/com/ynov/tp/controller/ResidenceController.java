package com.ynov.tp.controller;

import com.ynov.tp.domain.Appartment;
import com.ynov.tp.domain.Residence;
import com.ynov.tp.domain.AppartmentDTO;
import com.ynov.tp.domain.ResidenceDTO;
import com.ynov.tp.service.ResidenceService;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/rest")
public class ResidenceController {

    ResidenceService residenceService;
    public ResidenceController(ResidenceService residenceService){
        this.residenceService = residenceService;
    }

    @GetMapping("residencePrintById/{ResidenceId}")
    @ResponseStatus(HttpStatus.OK)
    public Residence residencePrintById(@PathVariable Long ResidenceId){
        return residenceService.residencePrintById(ResidenceId);
    }

    @GetMapping("appartmentPrintById/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Appartment appartmentPrintById(@PathVariable Long id){
        return residenceService.appartmentPrintById(id);
    }

    @GetMapping("residencePrintByName/{ResidenceName}")
    @ResponseStatus(HttpStatus.OK)
    public Residence getResidenceNameByName(@PathVariable String ResidenceName){
        return residenceService.getResidenceNameByName(ResidenceName);
    }

    @GetMapping("residencePrintByAppartment")
    @ResponseStatus(HttpStatus.OK)
    public List<Residence> getResidenceWithAppartment(){
        return residenceService.getResidenceWithAppartment();
    }

    @GetMapping("residencePrintByCountry/{Country}")
    @ResponseStatus(HttpStatus.OK)
    public List<Residence> getResidenceCountry(@PathVariable String Country){
        return residenceService.getResidenceCountry(Country);
    }

    @GetMapping("appartmentPrintByRegion/{Region}")
    @ResponseStatus(HttpStatus.OK)
    public List<Appartment> searchAppartmentRegion(@PathVariable String Region){
        return residenceService.searchAppartmentRegion(Region);
    }

    @GetMapping("appartmentPrintByPool")
    @ResponseStatus(HttpStatus.OK)
    public List<Appartment> searchAppartmentPool(){
        return residenceService.searchAppartmentPool();
    }

    @GetMapping("appartmentPrintByMountain")
    @ResponseStatus(HttpStatus.OK)
    public List<Appartment> searchAppartmentMountain(){
        return residenceService.searchAppartmentMountain();
    }



    @PostMapping("ResidenceGenerate")
    @ResponseStatus(HttpStatus.OK)
    public void generateResidence(){
        residenceService.generateResidence();
    }

    @PostMapping("ResidenceCreate")
    @ResponseStatus(HttpStatus.OK)
    public Residence createResidence(Residence residenceRequest){
        return residenceService.createResidence(residenceRequest);
    }

    @GetMapping("residences")
    @ResponseStatus(HttpStatus.OK)
    public List<Residence> ResidenceTab(){
        return residenceService.ResidenceTab();
    }

    @GetMapping("residence-{ResidenceId}")
    @ResponseStatus(HttpStatus.OK)
    public Residence showResidence(@PathVariable Long ResidenceId){
        return residenceService.showResidence(ResidenceId);
    }

    @PatchMapping("residence-{id}")
    @ResponseStatus(HttpStatus.OK)
    public Residence updateResidence(@PathVariable Long ResidenceId, Residence residenceRequest){
        return residenceService.updateResidence(ResidenceId, residenceRequest);
    }

    @DeleteMapping("residence-{id}")
    @ResponseStatus(HttpStatus.OK)
    public Residence deleteResidence(Long ResidenceId){
        return residenceService.deleteResidence(ResidenceId);
    }



    @PostMapping("appartmentGenerate")
    @ResponseStatus(HttpStatus.OK)
    public void generateAppartement(){
        residenceService.generateAppartment();
    }

    @PostMapping("appartmentCreate")
    @ResponseStatus(HttpStatus.OK)
    public Appartment createAppartment(Appartment appartmentRequest){
        return residenceService.createAppartment(appartmentRequest);
    }

    @GetMapping("appartment")
    @ResponseStatus(HttpStatus.OK)
    public List<Appartment> AppartmentTab(){
        return residenceService.AppartmentTab();
    }

    @GetMapping("appartment-{id}")
    @ResponseStatus(HttpStatus.OK)
    public Appartment showAppartment(@PathVariable Long id){
        return residenceService.showAppartment(id);
    }

    @PatchMapping("appartment-{id}")
    @ResponseStatus(HttpStatus.OK)
    public Appartment updateAppartment(@PathVariable Long id, Appartment appartmentRequest){
        return residenceService.updateAppartment(id, appartmentRequest);
    }

    @DeleteMapping("appartment-{id}")
    @ResponseStatus(HttpStatus.OK)
    public Appartment deleteAppartment(@PathVariable Long id){
        return residenceService.deleteAppartment(id);
    }

}

