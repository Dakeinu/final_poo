package com.ynov.tp.repository;

import com.ynov.tp.domain.AppartmentDTO;
import com.ynov.tp.domain.Residence;
import com.ynov.tp.domain.Appartment;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository

public interface AppartmentRepository extends JpaRepository<Appartment, Long> {

    @Query(value = "SELECT a FROM Appartment a LEFT JOIN Residence r WHERE r.Region = :Region ")
    List<Appartment> searchAppartmentRegion(String Region);

    @Query(value = "SELECT a FROM Appartment a LEFT JOIN Residence r WHERE r.Mountain = true ")
    List<Appartment> searchAppartmentMountain();

    @Query(value = "SELECT a FROM Appartment a LEFT JOIN Residence r WHERE r.Pool = true ")
    List<Appartment> searchAppartmentPool();

}
