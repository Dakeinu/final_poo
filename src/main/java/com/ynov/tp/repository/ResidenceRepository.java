package com.ynov.tp.repository;

import com.ynov.tp.domain.Residence;
import com.ynov.tp.domain.Appartment;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository

public interface ResidenceRepository extends JpaRepository<Residence, Long> {

    @Query(value = "SELECT r FROM Residence r JOIN FETCH Appartment a")
    List<Residence> getResidenceWithAppartment();

    @Query(value = "SELECT r FROM Residence r WHERE r.ResidenceName = :ResidenceName ")
    Residence getResidenceNameByName(String ResidenceName);

    @Query(value = "SELECT r FROM Residence r WHERE r.Country = :Country")
    List<Residence> getResidentCountry(String Country);


}
