package com.ynov.tp.domain;

import java.util.Set;

public class AppartmentDTO {

    private Long id;
    private int RoomNumber;
    private int NbSleeping;
    private String Surface;
    private boolean BabyTools;
    private boolean AirConditioning;
    private int DayCost;
    private String InDate;
    private String OutDate;

    private Set<Residence> residences;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public int getRoomNumber() {
        return RoomNumber;
    }
    public void setRoomNumber(int roomNumber) {
        RoomNumber = roomNumber;
    }

    public int getNbSleeping() {
        return NbSleeping;
    }
    public void setNbSleeping(int nbSleeping) {
        NbSleeping = nbSleeping;
    }

    public String getSurface() {
        return Surface;
    }
    public void setSurface(String surface) {
        Surface = surface;
    }

    public boolean isBabyTools() {
        return BabyTools;
    }
    public void setBabyTools(boolean babyTools) {
        BabyTools = babyTools;
    }

    public boolean isAirConditioning() {
        return AirConditioning;
    }
    public void setAirConditioning(boolean airConditioning) {
        AirConditioning = airConditioning;
    }

    public int getDayCost() {
        return DayCost;
    }
    public void setDayCost(int dayCost) {
        DayCost = dayCost;
    }

    public String getInDate() {
        return InDate;
    }
    public void setInDate(String inDate) {
        InDate = inDate;
    }

    public String getOutDate() {
        return OutDate;
    }
    public void setOutDate(String outDate) {
        OutDate = outDate;
    }

    public AppartmentDTO(Long id, int RoomNumber, int NbSleeping, String Surface, boolean BabyTools, boolean AirConditioning, int DayCost, String InDate, String OutDate) {
        this.id = id;
        this.RoomNumber = RoomNumber;
        this.NbSleeping = NbSleeping;
        this.Surface = Surface;
        this.BabyTools = BabyTools;
        this.AirConditioning = AirConditioning;
        this.DayCost = DayCost;
        this.InDate = InDate;
        this.OutDate = OutDate;
    }
}
