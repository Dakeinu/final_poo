package com.ynov.tp.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"RoomNumber","NbSleeping","BabyTools","Surface","AirConditioning","DayCost","InDate","OutDate"})})
public class Appartment {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)

    private Long id;
    private int RoomNumber;
    private int NbSleeping;
    private String Surface;
    private boolean BabyTools;
    private boolean AirConditioning;
    private int DayCost;
    private Date InDate;
    private Date OutDate;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public int getRoomNumber() {
        return RoomNumber;
    }
    public void setRoomNumber(int roomNumber) {
        RoomNumber = roomNumber;
    }

    public int getNbSleeping() {
        return NbSleeping;
    }
    public void setNbSleeping(int nbSleeping) {
        NbSleeping = nbSleeping;
    }

    public String getSurface() {
        return Surface;
    }
    public void setSurface(String surface) {
        Surface = surface;
    }

    public boolean isBabyTools() {
        return BabyTools;
    }
    public void setBabyTools(boolean babyTools) {
        BabyTools = babyTools;
    }

    public boolean isAirConditioning() {
        return AirConditioning;
    }

    public void setAirConditioning(boolean airConditioning) {
        AirConditioning = airConditioning;
    }

    public int getDayCost() {
        return DayCost;
    }
    public void setDayCost(int dayCost) {
        DayCost = dayCost;
    }

    public Date getInDate() {
        return InDate;
    }
    public void setInDate(Date inDate) {
        InDate = inDate;
    }

    public Date getOutDate() {
        return OutDate;
    }
    public void setOutDate(Date outDate) {
        OutDate = outDate;
    }


}
