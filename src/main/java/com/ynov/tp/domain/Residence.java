package com.ynov.tp.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Residence {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long ResidenceId;
    public String ResidenceName;
    public String Country;
    public String Region;
    public String Address;
    public String GPS;
    public boolean Sea;
    public boolean Ocean;
    public boolean Mountain;
    public boolean Pool;
    public boolean Spa;
    public boolean ChildCare;
    public boolean Wifi;

    @OneToMany
    @JoinColumn(name = "Residence_Id")

    private Set<Appartment> appartments;


    public Long getResidenceId() {
        return ResidenceId;
    }
    public void setResidenceId(Long residenceId) {
        ResidenceId = residenceId;
    }

    public String getResidenceName() {
        return ResidenceName;
    }
    public void setResidenceName(String residenceName) {
        ResidenceName = residenceName;
    }

    public String getCountry() {
        return Country;
    }
    public void setCountry(String country) {
        Country = country;
    }

    public String getRegion() {
        return Region;
    }
    public void setRegion(String region) {
        Region = region;
    }

    public String getAddress() {
        return Address;
    }
    public void setAddress(String address) {
        Address = address;
    }

    public String getGPS() {
        return GPS;
    }
    public void setGPS(String GPS) {
        this.GPS = GPS;
    }

    public boolean isSea() {
        return Sea;
    }
    public void setSea(boolean sea) {
        Sea = sea;
    }

    public boolean isOcean() {
        return Ocean;
    }
    public void setOcean(boolean ocean) {
        Ocean = ocean;
    }

    public boolean isMountain() {
        return Mountain;
    }
    public void setMountain(boolean mountain) {
        Mountain = mountain;
    }

    public boolean isPool() {
        return Pool;
    }
    public void setPool(boolean pool) {
        Pool = pool;
    }

    public boolean isSpa() {
        return Spa;
    }
    public void setSpa(boolean spa) {
        Spa = spa;
    }

    public boolean isChildCare() {
        return ChildCare;
    }
    public void setChildCare(boolean childCare) {
        ChildCare = childCare;
    }

    public boolean isWifi() {
        return Wifi;
    }
    public void setWifi(boolean wifi) {
        Wifi = wifi;
    }



}
