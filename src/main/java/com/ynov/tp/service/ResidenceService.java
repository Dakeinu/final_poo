package com.ynov.tp.service;

import com.ynov.tp.repository.AppartmentRepository;

import com.ynov.tp.domain.Residence;
import com.ynov.tp.domain.Appartment;
import com.ynov.tp.repository.ResidenceRepository;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class ResidenceService {

    private ResidenceRepository residenceRepository;
    private AppartmentRepository appartmentRepository;

    public ResidenceService(ResidenceRepository residenceRepository, AppartmentRepository appartmentRepository)
    {
        this.residenceRepository = residenceRepository;
        this.appartmentRepository = appartmentRepository;
    }

    public Residence residencePrintById(Long ResidenceId){
        return this.residenceRepository.findById(ResidenceId).get();
    }

    public Residence getResidenceNameByName(String ResidenceName){
        return residenceRepository.getResidenceNameByName(ResidenceName);
    }

    public List<Residence> getResidenceWithAppartment(){
        return residenceRepository.getResidenceWithAppartment();
    }

    public List<Residence> getResidenceCountry(String Country){
        return residenceRepository.getResidentCountry(Country);
    }


    public Appartment appartmentPrintById(Long id){
        return this.appartmentRepository.findById(id).get();
    }

    public List<Appartment> searchAppartmentRegion(String Region) {
        return appartmentRepository.searchAppartmentRegion(Region);
    }

    public List<Appartment> searchAppartmentPool(){
        return appartmentRepository.searchAppartmentPool();
    }

    public List<Appartment> searchAppartmentMountain(){
        return appartmentRepository.searchAppartmentMountain();
    }


    public void generateAppartment(){
        Appartment appartment = new Appartment();
        appartment.setRoomNumber(32);
        appartment.setNbSleeping(4);
        appartment.setSurface("52m²");
        appartment.setBabyTools(true);
        appartment.setAirConditioning(true);
        appartment.setDayCost(35);
        Date indate = new Date(2021-02-30);
        appartment.setInDate(indate);
        Date outdate = new Date(2021-04-04);
        appartment.setOutDate(outdate);
        appartmentRepository.save(appartment);

    }

    public Appartment createAppartment(Appartment appartmentRequest){
        Appartment appartment = new Appartment();
        appartment.setRoomNumber(appartmentRequest.getRoomNumber());
        appartment.setNbSleeping(appartmentRequest.getNbSleeping());
        appartment.setSurface(appartmentRequest.getSurface());
        appartment.setBabyTools(appartmentRequest.isBabyTools());
        appartment.setAirConditioning(appartmentRequest.isAirConditioning());

        appartment.setDayCost(appartmentRequest.getDayCost());
        appartment.setInDate(appartmentRequest.getInDate());
        appartment.setOutDate(appartmentRequest.getOutDate());
        this.appartmentRepository.save(appartment);
        return appartment;
    }

    public List<Appartment> AppartmentTab() {
        return this.appartmentRepository.findAll();
    }

    public Appartment showAppartment(Long id) {
        return this.appartmentPrintById(id);
    }

    public Appartment updateAppartment(Long id, Appartment appartmentRequest) {
        Appartment appartment = this.appartmentPrintById(id);
        appartment.setRoomNumber(appartmentRequest.getRoomNumber());
        appartment.setNbSleeping(appartmentRequest.getNbSleeping());
        appartment.setSurface(appartmentRequest.getSurface());
        appartment.setBabyTools(appartmentRequest.isBabyTools());
        appartment.setAirConditioning(appartmentRequest.isAirConditioning());

        appartment.setDayCost(appartmentRequest.getDayCost());
        appartment.setInDate(appartmentRequest.getInDate());
        appartment.setOutDate(appartmentRequest.getOutDate());

        this.appartmentRepository.save(appartment);
        return appartment;
    }

    public Appartment deleteAppartment(Long id) {
        Appartment appartment = this.appartmentPrintById(id);
        this.appartmentRepository.delete(appartment);
        return appartment;
    }


    public void generateResidence() {

        Residence residence = new Residence();
        residence.setResidenceName("Res1");
        residence.setCountry("France");
        residence.setRegion("Occitanie");
        residence.setGPS("Marseille");
        residence.setAddress("chemin du prat long");
        residence.setSea(true);
        residence.setOcean(false);
        residence.setMountain(false);
        residence.setPool(true);
        residence.setSpa(true);
        residence.setChildCare(true);
        residence.setWifi(true);
        residenceRepository.save(residence);
        Appartment appartment = new Appartment();
        appartment.setRoomNumber(19);
        appartment.setNbSleeping(2);
        appartment.setSurface("109m²");
        appartment.setBabyTools(true);
        appartment.setAirConditioning(true);
        appartment.setDayCost(50);
        Date indate = new Date(2021-02-19);
        appartment.setInDate(indate);
        Date outdate = new Date(2021-03-21);
        appartment.setOutDate(outdate);
        appartmentRepository.save(appartment);
    }

    public Residence createResidence(Residence residenceRequest){
        Residence residence = new Residence();
        residence.setResidenceName(residenceRequest.getResidenceName());
        residence.setPool(residenceRequest.isPool());
        residence.setSpa(residenceRequest.isSpa());
        residence.setChildCare(residenceRequest.isChildCare());
        residence.setWifi(residenceRequest.isWifi());
        residence.setSea(residenceRequest.isSea());
        residence.setOcean(residenceRequest.isOcean());
        residence.setMountain(residenceRequest.isMountain());

        residence.setCountry(residenceRequest.getCountry());
        residence.setRegion(residenceRequest.getRegion());
        residence.setAddress(residenceRequest.getAddress());
        residence.setGPS(residenceRequest.getGPS());

        this.residenceRepository.save(residence);
        return residence;
    }

    public List<Residence> ResidenceTab() {
        return this.residenceRepository.findAll();
    }

    public Residence showResidence(Long id) {
        return this.residencePrintById(id);
    }

    public Residence updateResidence(Long id, Residence residenceRequest) {
        Residence residence = this.residencePrintById(id);
        residence.setResidenceName(residenceRequest.getResidenceName());
        residence.setPool(residenceRequest.isPool());
        residence.setSpa(residenceRequest.isSpa());
        residence.setChildCare(residenceRequest.isChildCare());
        residence.setWifi(residenceRequest.isWifi());
        residence.setSea(residenceRequest.isSea());
        residence.setOcean(residenceRequest.isOcean());
        residence.setMountain(residenceRequest.isMountain());

        residence.setCountry(residenceRequest.getCountry());
        residence.setRegion(residenceRequest.getRegion());
        residence.setAddress(residenceRequest.getAddress());
        residence.setGPS(residenceRequest.getGPS());

        this.residenceRepository.save(residence);
        return residence;
    }

    public Residence deleteResidence(Long id) {
        Residence residence = this.residencePrintById(id);
        this.residenceRepository.delete(residence);
        return residence;
    }

}
